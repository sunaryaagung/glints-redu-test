const mongoose = require("mongoose");

module.exports = () => {
  const env = process.env.NODE_ENV || "development";

  if (env === "development" || env === "test") {
    require("dotenv").config();
  }
  const configDB = {
    development: process.env.DB_DEV,
    test: process.env.DB_TEST,
    production: process.env.DB_PROD
  };

  const dbConnection = configDB[env];

  mongoose
    .connect(dbConnection, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    })
    .then(() => console.log(`use DB ${dbConnection}`));
};
