const { Video } = require("../models/Video");
const { successResponse, errorResponse } = require("../helpers/response");

async function getAll(req, res) {
  const videos = await Video.find().sort("date");
  res.status(200).json(successResponse(videos));
}

async function getById(req, res) {
  const video = await Video.findById(req.params.id);
  if (!video) {
    return res
      .status(404)
      .json(errorResponse("Invalid id, video was not found"));
  }
  res.status(200).json(successResponse(video));
}

async function getByUser(req, res) {
  const videos = await Video.find({ _user: req.body._user });
  const msg = videos.length === 0 ? "There is no video" : "list of video";
  res.status(200).json(successResponse(videos, msg));
}

async function getByCategory(req, res) {
  const videos = await Video.find({ category: req.body.category });
  const msg = videos.length === 0 ? "There is no video" : "list of video";
  res.status(200).json(successResponse(videos, msg));
}

async function getMyVideo(req, res) {
  let videos = await Video.find({ _user: req.user._id });
  const msg = videos.length === 0 ? "There is no video" : "list of video";
  res.status(200).json(successResponse(videos));
}

async function addVideo(req, res) {
  let { title, description, url, category } = req.body;
  const video = new Video({
    _user: req.user._id,
    title,
    description,
    category,
    url
  });
  try {
    await video.save();
    res.status(201).json(successResponse(video));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function updateVideo(req, res) {
  try {
    let video = await Video.findOneAndUpdate(
      { _id: req.params.id, _user: req.user._id },
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(successResponse(video));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

async function deleteVideo(req, res) {
  try {
    let video = await Video.findOneAndRemove({
      _id: req.params.id,
      _user: req.user._id
    });
    res.status(200).json(successResponse(video, "deleted"));
  } catch (err) {
    res.status(400).json(errorResponse(err.message));
  }
}

module.exports = {
  getAll,
  getById,
  getByUser,
  getByCategory,
  getMyVideo,
  addVideo,
  updateVideo,
  deleteVideo
};
