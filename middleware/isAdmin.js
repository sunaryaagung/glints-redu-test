const { User } = require("../models/User");
const { errorResponse } = require("../helpers/response");

module.exports = async function(req, res, next) {
  let user = await User.findById(req.user._id);
  if (user.isAdmin === false) {
    return res.status(403).json(errorResponse("you are not an admin"));
  }
  next();
};
