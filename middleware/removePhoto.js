const { User } = require("../models/User");
const { Content } = require("../models/Content");
const { Community } = require("../models/Community");
const { Story } = require("../models/Story");
const cloudinary = require("cloudinary").v2;

async function destroyUser(req, res, next) {
  try {
    let user = await User.findById(req.user._id);
    cloudinary.uploader.destroy(user.photo.public_id, function(err, result) {
      console.log(result);
      next();
    });
  } catch (err) {
    /* istanbul ignore next */
    next();
  }
}

async function destroyContent(req, res, next) {
  try {
    let content = await Content.findById(req.params.id);
    cloudinary.uploader.destroy(content.photo.public_id, function(err, result) {
      console.log(result);
      next();
    });
  } catch (err) {
    next();
  }
}

async function destroyCommunity(req, res, next) {
  try {
    let community = await Community.findById(req.params.id);
    cloudinary.uploader.destroy(community.photo.public_id, function(
      err,
      result
    ) {
      console.log(result);
      next();
    });
  } catch (err) {
    next();
  }
}

async function destroyStory(req, res, next) {
  try {
    let story = await Story.findById(req.params.id);
    cloudinary.uploader.destroy(story.photo.public_id, function(err, result) {
      console.log(result);
      next();
    });
  } catch (err) {
    /* istanbul ignore next */
    next();
  }
}

module.exports = { destroyUser, destroyContent, destroyCommunity, destroyStory };
