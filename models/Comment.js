const mongoose = require("mongoose");

const commentSchema = new mongoose.Schema({
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
  _content: { type: mongoose.Schema.Types.ObjectId, ref: "contents" },
  _story: { type: mongoose.Schema.Types.ObjectId, ref: "stories" },
  _video: { type: mongoose.Schema.Types.ObjectId, ref: "stories" },
  text: { type: String },
  date: { type: Date, default: Date.now }
});

const Comment = new mongoose.model("comments", commentSchema);

exports.Comment = Comment;
