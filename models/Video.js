const mongoose = require("mongoose");

const videoSchema = new mongoose.Schema({
  _user: { type: mongoose.Schema.Types.ObjectId, ref: "users" },
  title: { type: String, required: true },
  description: { type: String },
  category: [{ type: String, lowercase: true }],
  url: { type: String, required: true },
  date: { type: Date, defualt: Date.now }
});

const Video = new mongoose.model("video", videoSchema);

exports.Video = Video;
