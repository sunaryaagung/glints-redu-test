const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const objectId = require("../middleware/objectId");
const upload = require("../middleware/multer");
const { destroyStory } = require("../middleware/removePhoto");

const { postStory, get, getStoryById, getStoryByUser, getStoryByCategory, updateStory, deleteStory, storyPict } = require("../controllers/storyControllers");

//story get
router.get("/page/:page", get); // get story
router.get("/detail/:id", objectId, getStoryById); // get story by id
router.get("/mine", auth, getStoryByUser); // get story by user
router.get("/filter/:category", getStoryByCategory)

//story POST/PUT/DELETE
router.post("/", auth, postStory); // add story
router.put("/update/:id", auth, updateStory); // update story
router.delete("/delete/:id", auth, objectId, destroyStory, deleteStory); // delete story

//Picture
router.put("/pict/:id",
    auth,
    objectId,
    cloudinaryConfig,
    upload.single("file"),
    storyPict
);

module.exports = router;
