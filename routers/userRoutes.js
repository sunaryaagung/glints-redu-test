const express = require("express");
const router = express.Router();
const auth = require("../middleware/auth");
const objectId = require("../middleware/objectId");
const cloudinaryConfig = require("../config/cloudinaryConfig");
const upload = require("../middleware/multer");
const { destroyUser } = require("../middleware/removePhoto");
const {
  get,
  add,
  userPict,
  update,
  login,
  getById,
  getCurrentUser,
  del
} = require("../controllers/userControllers");

router.use("*", cloudinaryConfig);

router.get("/", get);
router.get("/detail/:id", objectId, getById);
router.get("/me", auth, getCurrentUser);
router.post("/", add);
router.put("/pict", auth, upload.single("file"), userPict);
router.put("/update", auth, update);
router.post("/login", login);
router.delete("/del/:id", auth, objectId, destroyUser, del);
module.exports = router;
