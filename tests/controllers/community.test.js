process.env.NODE_ENV = "test";

const { Community } = require("../../models/Community");
const { User } = require("../../models/User");

const chai = require("chai");
const chaihttp = require("chai-http");
const should = chai.should();
const server = require("../../index");
const fs = require("fs");

chai.use(chaihttp);

let fakeAdmin = {
  photo: {
    public_id: "ysz6cc1nqyis8os8nvth",
    secure_url:
      "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
  },
  isAdmin: true,
  name: "admin",
  password: "admin",
  email: "admin@mail.com"
};

let fakeCommunity = {
  name: "test comm",
  url: "test url",
  location: "jakarta",
  photo: {
    public_id: "ysz6cc1nqyis8os8nvth",
    secure_url:
      "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
  }
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeAdmin);
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  user.save();
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
//delete all table before test
describe("Community", () => {
  beforeEach(done => {
    Community.deleteMany({}, { new: true }, err => {
      User.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });

  afterEach(done => {
    Community.deleteMany({}, { new: true }, err => {
      User.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });

  describe("/GET all Community", () => {
    it("It should get all community in db", done => {
      chai
        .request(server)
        .get("/api/communities")
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.an("object");
          done();
        });
    });
  });

  describe("/GET Root API", () => {
    it("It should get Root API", done => {
      chai
        .request(server)
        .get("/")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/GET community by id", () => {
    it("it should return community by the given id", done => {
      let community = new Community(fakeCommunity);
      community.save((err, res) => {
        chai
          .request(server)
          .get("/api/communities/detail/" + community._id)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET community by id", () => {
    it("it should return 404 if community is not found", done => {
      let community = new Community(fakeCommunity);
      community.save((err, res) => {
        chai
          .request(server)
          .get("/api/communities/detail/" + fakeId)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/GET By Location", () => {
    it("it should return find community by its location", done => {
      let community = new Community(fakeCommunity);
      community.save((err, res) => {
        chai
          .request(server)
          .get("/api/communities/filter/" + community.location)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET By Location", () => {
    it("it should return find community by its location", done => {
      let community = new Community(fakeCommunity);
      community.save((err, res) => {
        chai
          .request(server)
          .get("/api/communities/filter/a")
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/POST community", () => {
    it("it should add a new community to db", done => {
      let usr = login();
      chai
        .request(server)
        .post("/api/communities")
        .set("Authorization", usr.bear)
        .send({
          name: "test comm",
          url: "test url",
          location: "jakarta",
          photo: {
            public_id: "ysz6cc1nqyis8os8nvth",
            secure_url:
              "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
          }
        })
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });

  describe("/POST community", () => {
    it("it should return error 400 if req is bad", done => {
      let usr = login();
      let community = new Community(fakeCommunity);
      community.save((err, res) => {
        chai
          .request(server)
          .post("/api/communities")
          .set("Authorization", usr.bear)
          .end((err, res) => {
            res.should.have.status(400);
            done();
          });
      });
    });
  });

  describe("/PUT UPDATE COMMUNITY", () => {
    it("it should update a community in the db", done => {
      let comm = new Community(fakeCommunity);
      let user = login();
      comm.save((err, res) => {
        chai
          .request(server)
          .put("/api/communities/update/" + comm._id)
          .set("Authorization", user.bear)
          .send({ location: "t" })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT UPDATE COMMUNITY", () => {
    it("it should return error 404 if community is not found", done => {
      let comm = new Community(fakeCommunity);
      let user = login();
      comm.save((err, res) => {
        chai
          .request(server)
          .put("/api/communities/update/" + fakeId)
          .set("Authorization", user.bear)
          .send({ location: "t" })
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/DELETE COMMUNITY", () => {
    it("it should delete community from the db", done => {
      let comm = new Community(fakeCommunity);
      let user = login();
      comm.save((err, res) => {
        chai
          .request(server)
          .delete("/api/communities/delete/" + comm._id)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE COMMUNITY", () => {
    it("it should return error 404 if community is not found", done => {
      let comm = new Community(fakeCommunity);
      let user = login();
      comm.save((err, res) => {
        chai
          .request(server)
          .delete("/api/communities/delete/" + fakeId)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should update user picture", done => {
      let user = login();
      let comm = new Community(fakeCommunity);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/icon.png";
      comm.save((err, res) => {
        chai
          .request(server)
          .put("/api/communities/pict/" + comm._id)
          .set("Authorization", user.bear)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should should return error 428 if no file received", done => {
      let user = login();
      let comm = new Community(fakeCommunity);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/icon.png";
      comm.save((err, res) => {
        chai
          .request(server)
          .put("/api/communities/pict/" + comm._id)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should should return error 415 if file format is wrong", done => {
      let user = login();
      let comm = new Community(fakeCommunity);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/user.test.js";
      comm.save((err, res) => {
        chai
          .request(server)
          .put("/api/communities/pict/" + comm._id)
          .set("Authorization", user.bear)
          .attach("file", fs.readFileSync(`${file}`), "user.test.js")
          .end((err, res) => {
            res.should.have.status(415);
            done();
          });
      });
    });
  });
});
