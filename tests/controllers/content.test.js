process.env.NODE_ENV = "test";

const { User } = require("../../models/User");
const { Comment } = require("../../models/Comment");
const { Content } = require("../../models/Content");

const chai = require("chai");
const chaihttp = require("chai-http");
const should = chai.should();
const server = require("../../index");
const fs = require("fs");

chai.use(chaihttp);

let fakeAdmin = {
  photo: {
    public_id: "ysz6cc1nqyis8os8nvth",
    secure_url:
      "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
  },
  isAdmin: true,
  name: "admin",
  password: "admin",
  email: "admin@mail.com"
};

let fakeContent = {
  title: "test content title",
  body: "test content body",
  category: ["sport", "game"],
  photo: {
    public_id: "ysz6cc1nqyis8os8nvth",
    secure_url:
      "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
  }
};
let fakeId = "5d8129d830374d0016769548";

function login() {
  let user = new User(fakeAdmin);
  let token = user.generateToken();
  let bearer = `bearer ${token}`;
  user.save();
  let _user = user._id;
  return { bear: bearer, id: _user };
}

//PARENT BLOCK
//delete all table before test
describe("CONTENTS", () => {
  beforeEach(done => {
    Content.deleteMany({}, { new: true }, err => {
      User.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });

  afterEach(done => {
    Content.deleteMany({}, { new: true }, err => {
      User.deleteMany({}, { new: true }, err => {
        done();
      });
    });
  });

  describe("/GET CONTENT", () => {
    it("it should get all content in db", done => {
      chai
        .request(server)
        .get("/api/contents")
        .end((err, res) => {
          res.should.have.status(200);
          done();
        });
    });
  });

  describe("/GET BY ID", () => {
    it("it should find content with the given id", done => {
      let content = new Content(fakeContent);
      content.save((err, res) => {
        chai
          .request(server)
          .get("/api/contents/detail/" + content._id)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET BY ID", () => {
    it("it should return error 400 if content is not found", done => {
      chai
        .request(server)
        .get("/api/contents/detail/" + fakeId)
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/GET MY CONTENT", () => {
    it("it should return 404 if user has no content", done => {
      let user = login();
      let content = new Content({
        _user: fakeId,
        title: "test content title",
        body: "test content body",
        category: ["sport", "game"],
        photo: {
          public_id: "ysz6cc1nqyis8os8nvth",
          secure_url:
            "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
        }
      });
      content.save((err, res) => {
        chai
          .request(server)
          .get("/api/contents/mine")
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/GET MY CONTENT", () => {
    it("it should return all content by the current user", done => {
      let user = login();
      let content = new Content({
        _user: user.id,
        title: "test content title",
        body: "test content body",
        category: ["sport", "game"],
        photo: {
          public_id: "ysz6cc1nqyis8os8nvth",
          secure_url:
            "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
        }
      });
      content.save((err, res) => {
        chai
          .request(server)
          .get("/api/contents/mine")
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/GET BY CATEGORY", () => {
    it("it should return status 404 if there is not content by the given category", done => {
      chai
        .request(server)
        .get("/api/contents/filter/a")
        .end((err, res) => {
          res.should.have.status(404);
          done();
        });
    });
  });

  describe("/GET BY CATEGORY", () => {
    it("it should return all content by given category", done => {
      let content = new Content({
        _user: fakeId,
        title: "test content title",
        body: "test content body",
        category: ["sport", "game"],
        photo: {
          public_id: "ysz6cc1nqyis8os8nvth",
          secure_url:
            "https://res.cloudinary.com/sunaryaagung/image/upload/v1569573871/ysz6cc1nqyis8os8nvth.jpg"
        }
      });
      content.save((err, res) => {
        chai
          .request(server)
          .get("/api/contents/filter/game")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/POST CONTENT", () => {
    it("it should return error 400 if body is bad", done => {
      let user = login();
      chai
        .request(server)
        .post("/api/contents")
        .set("Authorization", user.bear)
        .send({ name: "" })
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe("/POST CONTENT", () => {
    it("it should return status 201 if content is created", done => {
      let user = login();
      chai
        .request(server)
        .post("/api/contents")
        .set("Authorization", user.bear)
        .send(fakeContent)
        .end((err, res) => {
          res.should.have.status(201);
          done();
        });
    });
  });

  describe("/PUT picture", () => {
    it("it should should return error 428 if no file received", done => {
      let user = login();
      let content = new Content(fakeContent);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/icon.png";
      content.save((err, res) => {
        chai
          .request(server)
          .put("/api/contents/pict/" + content._id)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(428);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should should return error 415 if file format is wrong", done => {
      let user = login();
      let content = new Content(fakeContent);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/user.test.js";
      content.save((err, res) => {
        chai
          .request(server)
          .put("/api/contents/pict/" + content._id)
          .set("Authorization", user.bear)
          .attach("file", fs.readFileSync(`${file}`), "user.test.js")
          .end((err, res) => {
            res.should.have.status(415);
            done();
          });
      });
    });
  });

  describe("/PUT picture", () => {
    it("it should update user picture", done => {
      let user = login();
      let content = new Content(fakeContent);
      let file = "/home/arya/binar/redu-mainan/tests/controllers/icon.png";
      content.save((err, res) => {
        chai
          .request(server)
          .put("/api/contents/pict/" + content._id)
          .set("Authorization", user.bear)
          .attach("file", fs.readFileSync(`${file}`), "icon.png")
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/PUT UPDATE CONTENT", () => {
    it("it should return err if content is not found", done => {
      let user = login();
      let content = new Content(fakeContent);
      content.save((err, res) => {
        chai
          .request(server)
          .put("/api/contents/update/" + content._id)
          .set("Authorization", user.bear)
          .send({ name: "" })
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/PUT UPDATE CONTENT", () => {
    it("it should update content by the given id", done => {
      let user = login();
      let content = new Content({
        title: "a",
        body: "a",
        _user: user.id
      });
      content.save((err, res) => {
        chai
          .request(server)
          .put("/api/contents/update/" + content._id)
          .set("Authorization", user.bear)
          .send({ title: "b" })
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });

  describe("/DELETE CONTENT", () => {
    it("it should return err 404 if content is not found", done => {
      let user = login();
      let content = new Content({
        title: "a",
        body: "b",
        _user: user.id
      });
      content.save((err, res) => {
        chai
          .request(server)
          .delete("/api/contents/delete/" + fakeId)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(404);
            done();
          });
      });
    });
  });

  describe("/DELETE CONTENT", () => {
    it("it should delete content by the given id", done => {
      let user = login();
      let content = new Content({
        title: "a",
        body: "b",
        _user: user.id
      });
      content.save((err, res) => {
        chai
          .request(server)
          .delete("/api/contents/delete/" + content._id)
          .set("Authorization", user.bear)
          .end((err, res) => {
            res.should.have.status(200);
            done();
          });
      });
    });
  });
});
